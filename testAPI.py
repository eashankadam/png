from flask import Flask,request,jsonify
import json
import requests
from flask import Flask
app = Flask(__name__)
### Todo monthly limit reached
@app.route("/png_question_and_answer")
def png_question_answer():
    # user=anjali.jatav@exponentia.ai
    # API_URL = "https://api-inference.huggingface.co/models/deepset/bert-base-cased-squad2"
    # headers = {"Authorization": "Bearer api_NLAafnQxLvrLEkCzIjkPKXvMQzAluXQkEY"}
    # user= anjalioffice20nov@gmail.com

    API_URL = "https://api-inference.huggingface.co/models/deepset/bert-base-cased-squad2"
    headers = {"Authorization": "Bearer api_GxNlIQUQfcncFOnvBwtzWrCCTRhgKNPxBK"}
    r = requests.get('http://127.0.0.1:5000/filecontext')
    # print("rrrrrrrrrrrrrrrrrrrrrrrr",print(r.text),r.text)
    context=r.text
    request_data={
        "inputs": {
            "question": "What is cyber grooming?",
            "context": context
            # "context": "CYBER BULLYING Cyber bullying is one of the common cyber threats being faced by children and young people. Though cyber bullying can impact anyone yet due to limited understanding about cyber threats, children become easy victims of cyber bullying. Cyber bullying means using internet or mobile technology to intentionally harass or bully someone by sending rude, mean or hurtful messages, comments and images/videos. A cyber bully can use text messages, emails, social media platforms, web pages, chat rooms, etc. to bully others. The consequences of cyber bullying on children are manifold. There can be physical, emotional and psychological consequences that can not only impact the academic performance of students but affect their daily life to a great extent. Concerned about cyber bullying? Don't worry... with E-awareness and precautions you can use internet and mobile technology without any fear. You need to be careful and follow safeguards to protect yourself and your friends against cyber bullying. Let's discuss how you can protect yourself from becoming a victim of cyber bullying Don't accept friend requests from unknown people on social media platforms. Cyber bully can even create a fake account to befriend victims. As a rule of the thumb, only add people online whom you know offline Don't share your personal information like date of birth, address, and phone number on social media or other online platforms. You can go to privacy settings on social media platforms to select who can access your posts online. Try to restrict access of your profile to your friends only. Picture stories for example on a particular platform are public by default. Remember what you post online remains there so it is important to be careful and not to share your phone number and other personal details in comments or posts on social media platforms Never install unwanted Software and Apps like dating App, online games, etc. from unknown sources. You should be very careful while chatting in the chat rooms. Never share personal details in the chat room and limit your identity. If you feel hurt after reading a post from a friend or a Stranger, don't react with aggressive reply. It may encourage the bully to keep posting such messages. If hurtful post/message is from your friend, you can request him not to do it again. If you are repeatedly getting such messages/ post, please inform your parents or elders immediately so that they can support you. Also, please remember that as a good netizen you should never share mean comments or hurtful messages or embarrassing pictures/videos online. Please be careful and check if your post/comment /videos can be embarrassing for your friend or anyone else. If so, please don't post. You should not become a cyber bully yourself as it is a punishable of fence. It adversely impacts the victim. If you feel that you are a victim of cyber bullying, please inform your elders so that they can intervene and support you. Following suggestions can be helpful in managing the situation. Inform your parents/elders immediately: If someone is bullying you, you must inform your parents/elders immediately. Don't feel that your parents will restrict your online activity or ask you not to use your computer/smartphone. It is important to inform them so that they can support and guide you. Narrate the entire issue clearly to your parents/elders. Identify the bully: Try to identify if the bully is a known person or a stranger. You should try to find out the reason why bully is bothering you. A bully can be your friend or a known person. You may seek help of your parents/teachers to reach out to the bully and ask him/her to stop bullying you. Block the Bully: If bully is using social media platforms to bully you, you can block him/her. All the social media apps or services have the option to block a user. Collect and Save posts/messages: Save posts/messages that were used against you. Such messages/posts can be used as an evidence, if in case a legal action has to be taken. Never respond to a bully aggressively: Bully wants you to get aggressive and get into heated argument. This adds mileage to the information unwantedly. So the best way is to ask the person politely to stop it and if he/she becomes annoying, stop the chat/ block him/ her If your parents/elders feel the need, they can contact local police station to lodge a complaint against the bully ",
        }
    }
    payload = json.dumps(request_data)
    response = requests.request("POST", API_URL, headers=headers, data=payload)
    print("response", response)
    final_response=json.loads(response.content.decode("utf-8"))
    print("dddddddddddddddddddddddddddd",final_response)
    return jsonify({"Answer": final_response['answer']}), 200

@app.route("/filecontext")
def file_content():
    # Read from text file
    # with open('CyberSecurity1.txt', encoding="utf-16") as f:
    #     context = f.read()
    #     print(context.rstrip("\n"))
    context="""
    CYBER BULLYING

Cyber bullying is one of the common cyber threats being faced by children and young people. Though cyber bullying can impact anyone yet due to limited understanding about cyber threats, children become easy victims of cyber bullying.

Cyber bullying means using internet or mobile technology to intentionally harass or bully someone by sending rude, mean or hurtful messages, comments and images/videos. A cyber bully can use text messages, emails, social media platforms, web pages, chat rooms, etc. to bully others. 

The consequences of cyber bullying on children are manifold. There can be physical, emotional and psychological consequences that can not only impact the academic performance of students but affect their daily life to a great extent.

Concerned about cyber bullying? Don't worry... with E-awareness and precautions you can use internet and mobile technology without any fear. You need to be careful and follow safeguards to protect yourself and your friends against cyber bullying.

Let's discuss how you can protect yourself from becoming a victim of cyber bullying

Don't accept friend requests from unknown people on social media platforms. Cyber bully can even create a fake account to befriend victims. As a rule of the thumb, only add people online whom you know offline

Don't share your personal information like date of birth, address, and phone number on social media or other online platforms. You can go to privacy settings on social media platforms to select who can access your posts online. Try to restrict access of your profile to your friends only. Picture stories for example on a particular platform are public by default.

Remember what you post online remains there so it is important to be careful and not to share your phone number and other personal details in comments or posts on social media platforms

Never install unwanted Software and Apps like dating App, online games, etc. from unknown sources. You should be very careful while chatting in the chat rooms. Never share personal details in the chat room and limit your identity.

If you feel hurt after reading a post from a friend or a
Stranger, don't react with aggressive reply. It may encourage
the bully to keep posting such messages. If hurtful
post/message is from your friend, you can request him not to
do it again. If you are repeatedly getting such messages/
post, please inform your parents or elders immediately so
that they can support you.

Also, please remember that as a good netizen you should never
share mean comments or hurtful messages or embarrassing
pictures/videos online. Please be careful and check if your
post/comment /videos can be embarrassing for your friend or
anyone else. If so, please don't post. You should not become a
cyber bully yourself as it is a punishable of fence. It adversely
impacts the victim.

If you feel that you are a victim of cyber bullying, please inform
your elders so that they can intervene and support you. Following
suggestions can be helpful in managing the situation.

Inform your parents/elders immediately: If someone is bullying
you, you must inform your parents/elders immediately. Don't
feel that your parents will restrict your online activity or ask you not to
use your computer/smartphone. It is important to inform them so that they can support and guide you. Narrate the entire issue clearly to your parents/elders.

Identify the bully: Try to identify if the bully is a known
person or a stranger. You should try to find out the reason why
bully is bothering you. A bully can be your friend or a known
person. You may seek help of your parents/teachers to reach
out to the bully and ask him/her to stop bullying you.

Block the Bully: If bully is using social media platforms to bully you, you can block
him/her. All the social media apps or services have the option to block a user.

 Collect and Save posts/messages: Save posts/messages that
were used against you. Such messages/posts can be used as an
evidence, if in case a legal action has to be taken.

Never respond to a bully aggressively: Bully wants you to get
aggressive and get into heated argument. This adds mileage to
the information unwantedly. So the best way is to ask the
person politely to stop it and if he/she becomes annoying, stop
the chat/ block him/ her

If your parents/elders feel the need, they can contact
local police station to lodge a complaint against the bully


Cyber Grooming

   

Cyber Grooming is growing as one of the major cyber
threats faced by children and teenagers. It is a practice where
someone builds an emotional bond with children through social
media or messaging platforms with an objective of gaining their
trust for sexually abusing or exploiting them.

The cyber groomers can use gaming websites, social media, email,
chat rooms, instant messaging, etc. by creating a fake account and
pretending to be a child or having same interests as of the child.

Do you know that many of us don't even realise that someone is grooming us online? Online groomer can be a known person, a relative or even an unknown person whom we met online on social media platform, a chatroom or a gaming portal.

Initially, the cyber groomer can give you compliments, gifts, modelling job offer and later they can start sending obscene messages, photographs or videos and will ask you to share your sexually explicit images or videos with them.


The online groomer mostly target teenagers as in adolescence as they face immense biological, personal and social changes. The impulsive and curious nature of adolescents encourages them to
engage in online activities which makes them vulnerable to online
grooming.

The cyber grooming has deep impact on a child's physical, emotional as well as psychological well-being. It can not only impact their academic performance but also their daily life to a great extent. The devastating effects of online grooming can sometimes be long-
term and can even haunt the victim in their adulthood


Concerned about cyber grooming? Don't worry... with awareness and precautions you can use internet and mobile technology without any fear. You need to be careful and follow safeguards to protect yourself and your friends against cyber grooming.

Let's discuss how you can protect yourself from becoming a victim of cyber grooming

Don't accept friend request from unknown people on social media platforms. Cyber groomer can even create a fake
account to befriend victims.

Don't share your personal information like date of birth, address, phone number and school name on social media or other online platforms. You can go to privacy settings on social
media platforms to select who can access your posts online.
Try to restrict access of your profile to your friends only.

Be cautious when your chat partner gives you many compliments regarding your appearance in just a short span of
your acquaintance.

Avoid talking to people who asks you questions related to your physical or sexual experiences. You can tell the person to stop
asking you such questions as you feel uncomfortable. If they continue to do the same, immediately inform your parents.

(r)

Do not talk to people who ask you to share your sexually explicit photographs or videos. If you share your sexually explicit photos or videos with someone, the person can share
those photos with others or post them on social media. They can also blackmail you.

Never turn on your webcam while your chat partner does not connects to the webcam

Talk to your elders or parents, if your chat partner suggests to keep your conversation with them a secret.

Do not go to meet a person whom you met online alone. Always take a friend or an elder person with you.

Never install unwanted Software and Apps like dating App, online games, etc. from unknown sources. You should be very
careful while chatting in the chat rooms. Never share personal details in the chat room and limit your identity.

If you feel that you are a victim of cyber grooming, please inform your elders so that they can intervene and support you. Following
suggestions can be helpful in managing the situation.

Inform your parents / elders
immediately: If someone online is
making you uncomfortable, you must
inform your parents / elders immediately. Don't feel that your parents will restrict your online activity  or ask you not to use your computer 
smart phone. It is important to inform a
them so that they can support and guide you. Narrate the entire issue clearly to your parents/elders.

 

Block the Groomer: If groomer is using social media platforms to groom you, you can block him/her. All the social media apps or
services have the option to block a user.

Collect and Save messages: Save messages, pictures or videos shared with you by the groomer. Such messages, pictures or
videos can be used as an evidence to take a legal action against them.

Your parents/elders can contact local police station to lodge a complaint against the groomer.

Do you know that producing, publishing and transmitting sexually explicit material or Child Sexual Abuse Material(CSAM) is electronic form is a punishable offence under the Information Technology Act 2000 of India?

    """
    # return jsonify({"data": context}), 200
    return (context), 200

if __name__ == "__main__":
    app.run(debug=True,host='127.0.0.1',port=5000)