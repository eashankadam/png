
# Requirements
# !pip install tensorflow
# pip install transformers

# from transformers import BertForQuestionAnswering, AutoTokenizer
# from transformers import AutoTokenizer, AutoModelForQuestionAnswering

from transformers import AutoTokenizer, AutoModelForQuestionAnswering
# tokenizer = AutoTokenizer.from_pretrained("deepset/bert-large-uncased-whole-word-masking-squad2")
# model = AutoModelForQuestionAnswering.from_pretrained("deepset/bert-large-uncased-whole-word-masking-squad2")

from transformers import BertForQuestionAnswering, AutoTokenizer

# def png():
modelname = 'deepset/bert-base-cased-squad2'
model = BertForQuestionAnswering.from_pretrained(modelname)
tokenizer = AutoTokenizer.from_pretrained(modelname)

from transformers import pipeline
nlp = pipeline('question-answering', model=model, tokenizer=tokenizer)
with open('CyberSecurity1.txt', encoding="utf-16") as f:
    context = f.read()
    print(context.rstrip("\n"))

nlp({
    'question': 'how you can protect yourself from becoming a victim of cyber bullying?',
    'context': context
})

########################
#### todo Azure qnA maker method
#################
# import requests
# import json
#
# url = "https://anjali-80ca.azurewebsites.net/qnamaker/knowledgebases/d96e8948-ddbe-4f64-89a9-1d699b3909b0/generateAnswer"
#
# payload = json.dumps({
#   "question": "how online gaming is related to cyber security?"
# })
# headers = {
#   'Content-Type': 'application/json',
#   'Host': 'anjali-80ca.azurewebsites.net',
#   'Content-Length': '37',
#   'X-Amz-Date': '20210416T064846Z',
#   'Authorization': 'EndpointKey 17457209-2c25-4c9c-8cba-49471b68ba04',
#   'Cookie': 'ARRAffinity=fd22269b4035216c397e2f27e7e0262aed521f7b39dabd3834f2d28a5a995f96; ARRAffinitySameSite=fd22269b4035216c397e2f27e7e0262aed521f7b39dabd3834f2d28a5a995f96'
# }
#
# response = requests.request("POST", url, headers=headers, data=payload)
#
# print(response.text)
